package com.example.usermanagement;

import com.example.usermanagement.users.User;
import com.example.usermanagement.users.UserController;
import com.example.usermanagement.users.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserManagementApplicationTests {

	User mockUser = new User(1, "Jerin", 34);

	@Autowired
	MockMvc mockMvc;

	@MockBean
	UserService userService;

	@Test
	public void contextLoads() throws Exception {
		String expected = "{ id:1,name:Jerin,age:34}";
		when(userService.getUser(1)).thenReturn(mockUser);
		MvcResult mvcResult = mockMvc.perform(
				MockMvcRequestBuilders
						.get("/users/1")
						.accept(MediaType.APPLICATION_JSON)
		).andReturn();

		System.out.println(mvcResult.getResponse());
		JSONAssert.assertEquals(expected, mvcResult.getResponse().getContentAsString(), false);
	}


}
