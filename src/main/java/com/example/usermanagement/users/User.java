package com.example.usermanagement.users;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_account")
public class User {

   public User(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }
    @Id
    private int id;
    private String name;
    private int age;
}
